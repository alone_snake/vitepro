import fetch from '../util/http'

export function getUser () {
  return fetch({
    url: 'http://jsonplaceholder.typicode.com/users',
    methods: 'get'
  })
}